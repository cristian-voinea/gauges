var levers;
var initiatives;
var benefits;
var formatter = new Intl.NumberFormat("en-GB", {
    style: "currency",
    currency: "GBP",
    notation: "compact",
    compactDisplay: "short"
});
var disabledInitiativesIds = [];

function setTotalBudget(min, val) {
    document.getElementById("minBudget").value = formatter.format(min);
    document.getElementById("totalBudget").value = formatter.format(val);
}

var order = {
    "Essential Upgrades and Re-platforming": 1,
    "Core System Improvements": 2,
    "External Client Services": 3,
    "Process Management and Automation": 4,
    "Internal and External Collaboration": 5,
    "Self Service Reporting and Analytics": 6
};

$(document).ready(function () {
    // readData();

    // /// toggle sliders
    // $('#v0').click(function () {
    //     $('.sliders').removeClass('v1 v2');
    //     $('.sliders').addClass('v0');
    //     $('.content').removeClass('v2');
    //     $('.version button').removeClass('active');
    //     $(this).addClass('active');
    // });
    //
    // $('#v1').click(function () {
    //     $('.sliders').removeClass('v0 v2');
    //     $('.sliders').addClass('v1');
    //     $('.content').removeClass('v2');
    //     $('.version button').removeClass('active');
    //     $(this).addClass('active');
    // });
    //
    // $('#v2').click(function () {
    //     $('.sliders').removeClass('v0 v1');
    //     $('.sliders').addClass('v2');
    //     $('.content').addClass('v2');
    //     $('.version button').removeClass('active');
    //     $(this).addClass('active');
    // });

    // function readData() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState != 4) {
            $("body").append(
                '<div class="loading"><div class="lds-ripple"><div></div><div></div></div></div>'
            );
        }

        if (this.readyState == 4 && this.status == 200) {
            $(".loading").remove();

            var data = JSON.parse(this.responseText);
            initiatives = data.initiatives;
            // }

            buildLeversAndGaugesArrays(initiatives);

            // console.log("Levers: " + initiatives.length);

            var totalBudget = 0;
            var initialBudget = 0;
            var sliders = document.getElementsByClassName("sliders")[0];

            // sliders.innerHTML += '    <div class="totals">' +
            // '       <span class="title"> Total budget: </span>' +
            // '       <input type="text" class="sum" size="6" id="totalBudget"/>' +
            // '   </div>'

            // console.log(levers,'levers');
            levers.sort(function (i1, i2) {
                return order[i1.label] - order[i2.label];
            });

            var minGaugesValues = getInitialGaugesPercentageValues(levers);

            if (window.localStorage.getItem("b1")) {
                var storedInitiatives = [
                    window.localStorage.getItem("b1"),
                    window.localStorage.getItem("b2"),
                    window.localStorage.getItem("b3"),
                    window.localStorage.getItem("b4"),
                    window.localStorage.getItem("b5"),
                    window.localStorage.getItem("b6")
                ];
            }

            levers.forEach(function (lever, index, array) {
                var initialInitiative = "";
                if (storedInitiatives) {
                    initialInitiative = storedInitiatives[index];
                } else {
                    initialInitiative = lever.initiatives.length;
                }

                var initialEstimateValue = getInitialEstimates(lever);
                var str = leverHTMLTemplate
                    .replace("-label-", lever.label)
                    .replace("-AR-", index + 1)
                    .replace("-min-", lever.minLevel)
                    .replace("-max-", lever.initiatives.length)
                    .replace("-sliderMin-", "sliderMin" + (index + 1))
                    .replace("-minLabel-", formatter.format(initialEstimateValue))
                    .replace("-maxLabel-", formatter.format(lever.totalBudget))
                    .replace(
                        "-minPercentage-",
                        (initialEstimateValue / lever.totalBudget) * 100
                    )
                    .replace("-minValue-", minGaugesValues[index])
                    .replace("-value-", initialInitiative)
                    .replace("-step-", 1);

                sliders.innerHTML += str;
                totalBudget += lever.totalBudget;
            });

            sliders.innerHTML +=
                '<span class="desc">Use the sliders to see organisational impact</span>';
            sliders.innerHTML += '<span id="changes"></span>';

            updateUI();
            populateInitiativesTable(initiatives);

            function getInitialEstimates(lever) {
                var step = 0;
                var initialEstimate = 0;
                while (step < lever.minLevel) {
                    initialEstimate += lever.initiatives[step].costEstimate;
                    step++;
                }
                return initialEstimate;
            }

            function getInitialGaugesPercentageValues(levers) {
                var initialGaugesValues = [0, 0, 0, 0, 0, 0];
                var totalGaugesValues = [0, 0, 0, 0, 0, 0];
                var step = 0;
                while (step < 6) {
                    var currentGauge = step + 1;
                    var indicator = "b" + currentGauge;
                    levers.forEach(function (lever, index, array) {
                        var currentLeverValue = lever.minLevel;
                        lever.initiatives.forEach(function (initiative, index2, array2) {
                            if (index2 < currentLeverValue) {
                                initialGaugesValues[step] += initiative[indicator];
                            }
                            totalGaugesValues[step] += initiative[indicator];
                        });
                    });
                    step++;
                }
                var initialPercentage = [0, 0, 0, 0, 0, 0];
                initialGaugesValues.forEach((initialGauge, index) => {
                    initialPercentage[index] = Math.floor(
                        (initialGauge / totalGaugesValues[index]) * 100
                    );
                });
                return initialPercentage;
            }
        }
    };
    xmlhttp.open(
        "GET",
        "https://script.google.com/macros/s/AKfycbxDOrJ3NU6wJYYegK2KAmkFcPeGpoU90G3zja1bsLXmApN5_yA/exec",
        true
    );
    xmlhttp.send();
});

function formatMoney(number, decPlaces, decSep, thouSep) {
    (decPlaces = isNaN((decPlaces = Math.abs(decPlaces))) ? 2 : decPlaces),
        (decSep = typeof decSep === "undefined" ? "." : decSep);

    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(
        parseInt((number = Math.abs(Number(number) || 0).toFixed(decPlaces)))
    );
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return (
        sign +
        (j ? i.substr(0, j) + thouSep : "") +
        i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
        (decPlaces
            ? decSep +
            Math.abs(number - i)
                .toFixed(decPlaces)
                .slice(2)
            : "")
    );
}

function buildLeversAndGaugesArrays(initiatives) {
    levers = [];
    benefits = [
        {total: 0, min: 0},
        {total: 0, min: 0},
        {total: 0, min: 0},
        {total: 0, min: 0},
        {total: 0, min: 0},
        {total: 0, min: 0}
    ];
    var iID = 0;
    initiatives.forEach(function (initiative, index, array) {
        initiative.id = iID++;
        initiative.isSelected = true;

        var foundLever = false;
        var foundGauge = false;
        levers.forEach(function (lever, index2, array2) {
            if (initiative.lever == lever.label) {
                foundLever = true;
            }
        });

        if (!foundLever && initiative.lever) {
            levers[levers.length] = {
                label: initiative.lever,
                initiatives: [],
                totalBudget: 0,
                minBudget: 0,
                minLevel: 0
            };
        }

        benefits[0].total += initiative.b1;
        benefits[1].total += initiative.b2;
        benefits[2].total += initiative.b3;
        benefits[3].total += initiative.b4;
        benefits[4].total += initiative.b5;
        benefits[5].total += initiative.b6;
    });

    // set total budget
    initiatives.forEach(function (initiative, index, array) {
        levers.forEach(function (currentLever, index2, array2) {
            if (initiative.lever == currentLever.label) {
                currentLever.initiatives[currentLever.initiatives.length] = initiative;
                if (initiative.priority == 1) {
                    currentLever.minLevel += 1;
                    currentLever.minBudget += initiative.costEstimate;

                    benefits[0].min += (initiative.b1 * 100) / benefits[0].total;
                    benefits[1].min += (initiative.b2 * 100) / benefits[1].total;
                    benefits[2].min += (initiative.b3 * 100) / benefits[2].total;
                    benefits[3].min += (initiative.b4 * 100) / benefits[3].total;
                    benefits[4].min += (initiative.b5 * 100) / benefits[4].total;
                    benefits[5].min += (initiative.b6 * 100) / benefits[5].total;
                }

                currentLever.totalBudget += initiative.costEstimate;

                // console.log("Lever summary: " + currentLever.label +
                //     ", initiative: " + initiative.initiative +
                //     ", initiative.costEstimate: " + initiative.costEstimate +
                //     ", totalBudget: " + currentLever.totalBudget +
                //     ", minLevel: " + currentLever.minLevel);
            }
        });
    });

    // sort initiatives within each lever based on their priorities
    levers.forEach(function (currentLever, index, array) {
        // console.log("Lever summary: " + currentLever.label +
        //    ", totalBudget: " + currentLever.totalBudget +
        //    ", minLevel: " + currentLever.minLevel);

        currentLever.initiatives.sort(function (i1, i2) {
            return i1.priority - i2.priority;
        });
    });
}

var leverHTMLTemplate =
    '<div class="slider" style="padding-left: -minPercentage-%">' +
    '    <div class="minmax">' +
    '        <div class="min"><b>min:</b> <span id="-sliderMin-" min-value="-minValue-">-minLabel-</span></div>' +
    '        <div class="max"><b>max:</b> -maxLabel-</div>' +
    "    </div>" +
    "    <label>-label-</label>" +
    '    <input class="e-range" type="range" id="amountRange-AR-" min="-min-" max="-max-" value="-value-" step="-step-" oninput="updateUI(this.id, this.value)">' +
    '    <ul class="val">' +
    '        <li class="value-pref">aprox.</li>' +
    '        <li class="value">-maxLabel-</li>' +
    "    </ul>" +
    "</div>";
//
// var gaugeHTMLTemplate = '<div id="gauge1" class="radialProgressBar progress" data-fill="25">' +
//     '   <div class="overlay">' +
//     '       <span class="label">qqq</span>' +
//     '       <span class="value">25%</span>' +
//     '   </div>' +
//     '</div>';

function updateRowColor(id, active) {
    var activeColor = "#000";
    var inactiveColor = "#CCC";
    var color = active ? activeColor : inactiveColor;

    if (document.getElementById("pillar" + id)) {
        document.getElementById("pillar" + id).style.color = color;
        document.getElementById("department" + id).style.color = color;
        document.getElementById("lever" + id).style.color = color;
        document.getElementById("initiative" + id).style.color = color;
        document.getElementById("future" + id).style.color = color;
        document.getElementById("status" + id).style.color = color;
        document.getElementById("priority" + id).style.color = color;
        document.getElementById("cost" + id).style.color = color;
    }
}

function disableTableRows() {
    setInitialEnabledState();
    disabledInitiativesIds.forEach(disabledInitiativeId => {
        var initiativeRow = document.getElementById("row" + disabledInitiativeId);
        if (initiativeRow) {
            initiativeRow.classList.add("row-disabled");
        }
    });
}

function setInitialEnabledState() {
    disabledRows = document.getElementsByClassName("row-disabled");
    if (disabledRows) {
        Array.from(disabledRows).forEach(disabledRow => {
            disabledRow.classList.remove("row-disabled");
        });
    }
}

function updateUI(name, val) {
    var minBudget = 0;
    var totalBudget = 0;

    var gaugesValues = [0, 0, 0, 0, 0, 0];
    disabledInitiativesIds = [];
    levers.forEach(function (lever, index, array) {
        var currentLeverValue = parseInt($("#amountRange" + (index + 1)).val());

        window.localStorage.setItem("b1", parseInt($("#amountRange1").val()));
        window.localStorage.setItem("b2", parseInt($("#amountRange2").val()));
        window.localStorage.setItem("b3", parseInt($("#amountRange3").val()));
        window.localStorage.setItem("b4", parseInt($("#amountRange4").val()));
        window.localStorage.setItem("b5", parseInt($("#amountRange5").val()));
        window.localStorage.setItem("b6", parseInt($("#amountRange6").val()));

        lever.currentBudget = 0;

        lever.initiatives.forEach(function (initiative, index2, array2) {
            if (index2 < currentLeverValue) {
                initiative.isSelected = true;
                lever.currentBudget += initiative.costEstimate;
                gaugesValues[0] += initiative.b1;
                gaugesValues[1] += initiative.b2;
                gaugesValues[2] += initiative.b3;
                gaugesValues[3] += initiative.b4;
                gaugesValues[4] += initiative.b5;
                gaugesValues[5] += initiative.b6;
            } else {
                disabledInitiativesIds.push(initiative.id);
                initiative.isSelected = false;
            }
        });

        disableTableRows();
        document.getElementById(
            "sliderMin" + (index + 1)
        ).innerHTML = formatter.format(lever.minBudget);
        totalBudget += lever.currentBudget;
        minBudget += lever.minBudget;

        //console.log(lever.currentBudget);
        //console.log(lever.currentBudget + " @@@@@@@@@@@@@ " + lever.minLevel);
        $("#amountRange" + (index + 1))
            .next(".val")
            .find(".value")
            .empty()
            .append(formatter.format(lever.currentBudget));
    });
    var counts = 0;
    initiatives.forEach(function (i, x, y) {
        if (!i.isSelected) {
            counts++;
        }
    });
    //   console.log(">>>!! " + counts);
    // console.log(formatter.format(totalBudget))
    setTotalBudget(minBudget, totalBudget);

    gaugesValues.forEach(function (gaugeValue, index, array) {
        //console.log('initial2, total2', gaugeValue, benefits[index].total);
        var gaugePercentage = parseInt((gaugeValue * 100) / benefits[index].total);

        var minProgress = $("#sliderMin" + (index + 1)).attr("min-value");

        //console.log(minProgress);

        $("#gauge" + (index + 1))
            .attr("data-fill", gaugePercentage)
            .find(".overlay .value")
            .empty()
            .append(
                gaugePercentage + " (min: " + parseInt(benefits[index].min) + ")"
            ); // + "%");
        $("#minprogress" + (index + 1)).attr("data-fill", minProgress);
    });

    Array.from(document.getElementsByClassName("e-range")).forEach(function (
        element
    ) {
        var percentage =
            ((element.value - element.min) * 100) / (element.max - element.min);

        //console.log('max:' + element.max + 'val:' + element.value + '%:' + percentage);
        element.style.background =
            "linear-gradient(to right, #2F5594 0%, #00AF9E " +
            percentage +
            "%, #fff " +
            percentage +
            "%, #fff 100%";
    });
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var table;

function populateInitiativesTable(initiatives) {
    var columns = [
        {
            data: "pillar",
            title: "Pillar",
            render: function (data, type, row, meta) {
                return "<span id='pillar" + row.id + "'>" + data + "</span>";
            }
        },
        {
            data: "department",
            title: "Department",
            render: function (data, type, row, meta) {
                return "<span id='department" + row.id + "'>" + data + "</span>";
            }
        },
        {
            data: "initiative",
            title: "Business capability",
            render: function (data, type, row, meta) {
                return "<span id='initiative" + row.id + "'>" + data + "</span>";
            }
        },
        {
            data: "future",
            title: "Future tech capability",
            render: function (data, type, row, meta) {
                return "<span id='future" + row.id + "'>" + data + "</span>";
            }
        },
        {
            data: "lever",
            title: "Lever",
            render: function (data, type, row, meta) {
                return "<span id='lever" + row.id + "'>" + data + "</span>";
            }
        },
        {data: "isSelected", visible: false},
        {
            data: "status",
            title: "Status",
            render: function (data, type, row, meta) {
                return "<span id='status" + row.id + "'>" + data + "</span>";
            }
        },
        {
            data: "priority",
            title: "Priority",
            render: function (data, type, row, meta) {
                return (
                    "<span id='priority" +
                    row.id +
                    "' style='float:right'>" +
                    data +
                    "</span>"
                );
            }
        },
        {
            data: "costEstimate",
            title: "Cost estimate",
            render: function (data, type, row, meta) {
                return (
                    "<span id='cost" +
                    row.id +
                    "' style='float:right'>" +
                    formatter.format(data) +
                    "</span>"
                );
            }
        }
    ];

    // console.log(data);

    // columnNames = Object.keys(initiatives[0]);
    // for (var i in columnNames) {
    //     columns.push({
    //         data: columnNames[i],
    //         title: capitalizeFirstLetter(columnNames[i])
    //     });
    // }

    table = $("#initiatives").DataTable({
        paging: true,
        ordering: true,
        pageLength: 10,
        info: false,
        bFilter: false,
        searching: true,
        data: initiatives,
        orderCellsTop: true,
        language: {
            paginate: {
                previous: "&laquo;",
                next: "&raquo;"
            }
        },
        fixedHeader: {
            header: true,
            footer: true
        },
        columns: columns,
        createdRow: function (row, data, index) {
            row.id = "row" + data.id;
        },
        footerCallback: function (row, data, start, end, display) {
            setTimeout(() => {
                disableTableRows();
            }, 0);
        },
        initComplete: function (settings, json) {
            setTimeout(() => {
                disableTableRows();
            }, 0);
        }
    });

    // $('#initiatives thead tr').clone(true).appendTo( '#initiatives thead' );
    // $('#initiatives thead tr:eq(1) th').each( function (i) {
    //     var title = $(this).text();
    //     $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    //
    //     $( 'input', this ).on( 'keyup change', function () {
    //         console.log(this.value);
    //             table
    //                 .column(i)
    //                 .search( this.value )
    //                 .draw();
    //     } );
    // } );

    // half gauge
    /* Set radius for all circles */
    var r = 100;
    var circles = document.querySelectorAll(".circle");
    var total_circles = circles.length;
    for (var i = 0; i < total_circles; i++) {
        circles[i].setAttribute("r", r);
    }

    /* Set meter's wrapper dimension */
    var meter_dimension = r * 2 + 100;
    var wrapper = document.querySelector("#wrapper");
    wrapper.style.width = meter_dimension + "px";
    wrapper.style.height = meter_dimension + "px";

    /* Add strokes to circles  */
    var cf = 2 * Math.PI * r;
    var semi_cf = cf / 2;
    var semi_cf_1by4 = semi_cf / 4;
    var semi_cf_2by4 = semi_cf / 2;
    var semi_cf_3by4 = semi_cf_1by4 * 3;

    // var semi_cf_2by3 = semi_cf_1by3 * 2;
    document
        .querySelector("#outline_curves")
        .setAttribute("stroke-dasharray", semi_cf + "," + cf);
    document
        .querySelector("#low")
        .setAttribute("stroke-dasharray", semi_cf + "," + cf);
    document
        .querySelector("#avg")
        .setAttribute("stroke-dasharray", semi_cf_3by4 + "," + cf);
    document
        .querySelector("#high")
        .setAttribute("stroke-dasharray", semi_cf_2by4 + "," + cf);
    document
        .querySelector("#max")
        .setAttribute("stroke-dasharray", semi_cf_1by4 + "," + cf);
    document
        .querySelector("#outline_ends")
        .setAttribute("stroke-dasharray", 2 + "," + (semi_cf - 2));
    document
        .querySelector("#mask")
        .setAttribute("stroke-dasharray", semi_cf + "," + cf);

    /* Bind range slider event*/
    var slider = document.querySelector("#slider");
    var lbl = document.querySelector("#lbl");
    var mask = document.querySelector("#mask");
    var meter_needle = document.querySelector("#meter_needle");

    function range_change_event() {
        var percent = slider.value;
        var meter_value = semi_cf - (percent * semi_cf) / 100;
        mask.setAttribute("stroke-dasharray", meter_value + "," + cf);
        meter_needle.style.transform =
            "rotate(" + (270 + (percent * 180) / 100) + "deg)";
        lbl.textContent = percent + "%";
    }

    slider.addEventListener("input", range_change_event);
}
